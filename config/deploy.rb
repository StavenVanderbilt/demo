# config valid for current version and patch releases of Capistrano
lock "~> 3.10.0"

puts "=========================================================================="
puts "===============                 deploy.rb                 ================"
puts "=========================================================================="

set :application,   "demo"
set :repo_url,      "git@bitbucket.org:StavenVanderbilt/demo.git"

set :stages,        ["staging", "production"]
set :default_stage, "staging"

# Default value for :linked_files is []
#append :linked_files, 'config/database.yml', 'config/puma.rb', 'config/configuration.yml', 'config/secrets.yml'
append :linked_files, 'config/database.yml', 'config/puma.rb', 'config/configuration.yml', 'config/secrets.yml', 'config/nginx.conf'

# Default value for linked_dirs is []
append :linked_dirs, 'log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle'

# use rvm gemset rather than bundle path for working with launchctl
set :rvm_type, :user
set :rvm_ruby_version, "2.3.1@#{fetch(:application)}"
set :bundle_path, nil
set :bundle_binstubs, nil
set :bundle_flags, '--system'

## Defaults
set :format,        :pretty
set :log_level,     :debug
set :keep_releases, 5

set :pty,                 true
set :use_sudo,            false
set :puma_user,           fetch(:user)
set :puma_state,          -> { "#{shared_path}/tmp/pids/puma.state" }
set :puma_pid,            -> { "#{shared_path}/tmp/pids/puma.pid" }
set :puma_bind,           -> { "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock" }
set :puma_conf,           -> { "#{shared_path}/config/puma.rb" }
set :puma_access_log,     -> { "#{release_path}/log/puma.access.log" }
set :puma_error_log,      -> { "#{release_path}/log/puma.error.log" }
set :puma_role,           :app
set :puma_init_active_record, false
set :puma_preload_app, true

def branch_name(default_branch)
 branch = ENV.fetch('BRANCH', default_branch)
 if branch == '.'
   # current branch
   `git rev-parse --abbrev-ref HEAD`.chomp
 else
   branch
 end
end

set :branch, branch_name('master')

namespace :nginx do
 desc 'setup config for nginx server ( how to use: cap stage_environment nginx:set_config )'
 task :set_config do
   on roles(:web) do
     execute :sudo, "rm -f /etc/nginx/sites-enabled/*.conf"
     #execute :sudo, "ln -s #{fetch(:deploy_to)}/current/config/nginx.conf /etc/nginx/sites-enabled/#{fetch(:application)}.conf"
     execute :sudo, "ln -s #{fetch(:deploy_to)}/shared/config/nginx.conf /etc/nginx/sites-enabled/#{fetch(:application)}.conf"
     execute :sudo, "/usr/sbin/nginx -s quit || true"
     execute :sudo, "/usr/sbin/nginx"
   end
 end

 desc "Reload nginx ( how to use: cap stage_environment nginx:reload )"
 task :reload do
   on roles(:web) do
     execute :sudo, "service nginx reload"
   end
 end
end

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/var/www/my_app_name"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for local_user is ENV['USER']
# set :local_user, -> { `git config user.name`.chomp }

# Default value for keep_releases is 5
# set :keep_releases, 5

# Uncomment the following to require manually verifying the host key before first deploy.
# set :ssh_options, verify_host_key: :secure
